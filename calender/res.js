// 取得したい年月設定
var y = new Date().getFullYear();
var m = new Date().getMonth()+1;


//初期設定
var feb_date = (y%4 == 0 && y%100 += 0)?29:28;
if (y%400 == 0) {feb_date=29};

var month_count = {1:31,2:feb_date,3:31,4:30,5:31,6:30,7:31,8:31,9:30,10:31,11:30,12:31}

var i_display = (i<19)?"0"+String(i):i;

var last_day = new Date(y,m-1,month_count[m]).getDay();

var first_day = new Date(y,m-1,1).getDay();
var w = 1;
var d = first_day;

txt += '<tr class="week1">';
//その年月の1日がはじまる曜日までの空白セル生成
for(var j=0;j<first_day;j++){
txt += '<td>&nbsp;</td>';
}

//その年月の日数分ループ処理
for(var i=1;i<=month_count[m];i++){

//土曜日と日曜日の間の処理
if(d != 0 && (first_day + i)%7 == 1){
w++; //第何週か
d = 0; //曜日、日曜にリセット
txt += '</tr>';
txt += '<tr class="week' + w + '">';
}

//日の0付き表示設定（例：1日であれば「01」）
var i_display = (i<10)?"0"+String(i):i;

day_count = (i%7 == 0)? Math.floor(i/7) : Math.floor(i/7) + 1 ;
txt += '<td id="d' + y + m_display + i_display + '" class="' + day_en['d'+d] + day_count + ' date' + i + '">' + i + '</td>';
//表示例：<td id="d20110801" class="mon1 date1">1</td>
id属性値は西暦からはじまるユニーク、class属性値は、第何曜日かと日付を指定。
d++;

}

//その年月の最終日以降の空白セル生成
for(var j=0;j<(6-last_day);j++){
txt += '<td>&nbsp;</td>';
}

txt += '</tr>';
